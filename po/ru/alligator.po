# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the alligator package.
#
# Alexander Yavorsky <kekcuha@gmail.com>, 2020, 2022.
# Alexander Potashev <aspotashev@gmail.com>, 2020.
# Olesya Gerasimenko <translation-team@basealt.ru>, 2022.
msgid ""
msgstr ""
"Project-Id-Version: alligator\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-02-22 00:58+0000\n"
"PO-Revision-Date: 2022-10-26 15:08+0300\n"
"Last-Translator: Olesya Gerasimenko <translation-team@basealt.ru>\n"
"Language-Team: Basealt Translation Team\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 22.04.3\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Олеся Герасименко"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "translation-team@basealt.ru"

#: database.cpp:67 database.cpp:335
#, kde-format
msgid "Default"
msgstr "По умолчанию"

#: database.cpp:68 database.cpp:336
#, kde-format
msgid "Default Feed Group"
msgstr "Группа лент по умолчанию"

#: fetcher.cpp:236
#, kde-format
msgid "Invalid XML"
msgstr "Некорректный XML"

#: fetcher.cpp:238
#, kde-format
msgid "No parser accepted the XML"
msgstr "XML не принят анализатором"

#: fetcher.cpp:240
#, kde-format
msgid "Error while parsing feed"
msgstr "Ошибка при разборе ленты"

#: main.cpp:78 qml/AlligatorGlobalDrawer.qml:57
#, kde-format
msgid "Alligator"
msgstr "Alligator"

#: main.cpp:80
#, kde-format
msgid "Feed Reader"
msgstr "Чтение лент новостей"

#: main.cpp:82
#, fuzzy, kde-format
#| msgid "© 2020 KDE Community"
msgid "© 2020-2022 KDE Community"
msgstr "© Сообщество KDE, 2020"

#: main.cpp:83
#, kde-format
msgid "Tobias Fella"
msgstr "Tobias Fella"

#: main.cpp:106
#, kde-format
msgid "RSS/Atom Feed Reader"
msgstr "Чтение лент новостей RSS/Atom"

#: main.cpp:108
#, kde-format
msgid "Adds a new feed to database."
msgstr "Добавляет в базу данных новую ленту."

#: main.cpp:109
#, kde-format
msgid "feed URL"
msgstr "URL-адрес ленты новостей"

#: qml/AddFeedDialog.qml:18
#, kde-format
msgid "Add Feed"
msgstr "Добавление ленты новостей"

#: qml/AddFeedDialog.qml:30
#, kde-format
msgid "Url:"
msgstr "URL-адрес:"

#: qml/AddFeedDialog.qml:40
#, kde-format
msgid "Mark entries as read"
msgstr "Отметить записи как прочитанные"

#: qml/AlligatorGlobalDrawer.qml:88
#, kde-format
msgid "All Feeds"
msgstr "Все ленты"

#: qml/AlligatorGlobalDrawer.qml:137 qml/SettingsPage.qml:15
#, kde-format
msgid "Settings"
msgstr "Параметры"

#: qml/AlligatorGlobalDrawer.qml:148
#, kde-format
msgid "Manage Feeds"
msgstr ""

#: qml/AlligatorGlobalDrawer.qml:159
#, kde-format
msgid "About"
msgstr "О программе"

#: qml/EditFeedDialog.qml:15
#, kde-format
msgid "Edit Feed"
msgstr "Редактирование ленты"

#: qml/EditFeedDialog.qml:35
#, kde-format
msgid "Display Name:"
msgstr "Отображаемое имя:"

#: qml/EditFeedDialog.qml:45
#, kde-format
msgid "Group:"
msgstr "Группа:"

#: qml/EntryListDelegate.qml:51
#, kde-format
msgctxt "by <author(s)>"
msgid "by"
msgstr " "

#: qml/EntryListPage.qml:44
#, kde-format
msgid "Details"
msgstr "Подробности"

#: qml/EntryListPage.qml:55
#, kde-format
msgid "Refresh"
msgstr "Обновить"

#: qml/EntryListPage.qml:64
#, kde-format
msgid "Expand sidebar"
msgstr ""

#: qml/EntryListPage.qml:64
#, kde-format
msgid "Collapse sidebar"
msgstr ""

#: qml/EntryListPage.qml:72
#, kde-format
msgid "All Entries"
msgstr "Все записи"

#: qml/EntryListPage.qml:80
#, kde-format
msgid "Error"
msgstr "Ошибка"

#: qml/EntryListPage.qml:81 qml/EntryListPage.qml:93
#, kde-format
msgid "Error (%1): %2"
msgstr "Ошибка (%1): %2"

#: qml/EntryListPage.qml:91
#, kde-format
msgid "No unread entries available"
msgstr "Нет доступных непрочитанных записей"

#: qml/EntryListPage.qml:91
#, kde-format
msgid "No entries available"
msgstr "Нет доступных записей"

#: qml/EntryListPage.qml:130
#, kde-format
msgid "All"
msgstr ""

#: qml/EntryListPage.qml:136
#, fuzzy, kde-format
#| msgid "Only Unread"
msgid "Unread"
msgstr "Только непрочитанные"

#: qml/EntryPage.qml:43
#, kde-format
msgid "Open in Browser"
msgstr "Открыть в браузере"

#: qml/FeedDetailsPage.qml:20
#, kde-format
msgctxt "<Feed Name> - Details"
msgid "%1 - Details"
msgstr "%1 — Подробности"

#: qml/FeedDetailsPage.qml:36
#, kde-format
msgctxt "by <author(s)>"
msgid "by %1"
msgstr "%1"

#: qml/FeedDetailsPage.qml:44
#, kde-format
msgid "Subscribed since: %1"
msgstr "Дата подписки: %1"

#: qml/FeedDetailsPage.qml:47
#, kde-format
msgid "last updated: %1"
msgstr "Последнее обновление: %1"

#: qml/FeedDetailsPage.qml:50
#, kde-format
msgid "%1 posts, %2 unread"
msgstr "Записей — %1, не прочитано — %2"

#: qml/FeedGroupDialog.qml:23
#, kde-format
msgid "Feed Group"
msgstr "Группа лент новостей"

#: qml/FeedGroupDialog.qml:43
#, kde-format
msgid "Name:"
msgstr "Название:"

#: qml/FeedGroupDialog.qml:50
#, kde-format
msgid "Description:"
msgstr "Описание:"

#: qml/FeedListDelegate.qml:21
#, kde-format
msgid "%1 unread entry"
msgid_plural "%1 unread entries"
msgstr[0] "%1 непрочитанная запись"
msgstr[1] "%1 непрочитанные записи"
msgstr[2] "%1 непрочитанных записей"
msgstr[3] "%1 непрочитанная запись"

#: qml/FeedListDelegate.qml:30
#, fuzzy, kde-format
#| msgid "Delete after:"
msgctxt "'Feed' is an rss feed"
msgid "Delete this Feed"
msgstr "Удалить через:"

#: qml/FeedListDelegate.qml:43
#, fuzzy, kde-format
#| msgid "Edit Feed"
msgctxt "'Feed' is an rss feed"
msgid "Edit this Feed"
msgstr "Редактирование ленты"

#: qml/FeedListPage.qml:18
#, fuzzy, kde-format
#| msgid "Feed Groups"
msgctxt "'Feeds' as in 'RSS Feeds'"
msgid "Manage Feeds"
msgstr "Группы лент новостей"

#: qml/FeedListPage.qml:29
#, kde-format
msgid "Refresh All Feeds"
msgstr "Обновить все ленты"

#: qml/FeedListPage.qml:35
#, fuzzy, kde-format
#| msgid "Feed Groups"
msgid "Manage Feed Groups"
msgstr "Группы лент новостей"

#: qml/FeedListPage.qml:40
#, fuzzy, kde-format
#| msgid "Import Feeds"
msgid "Import Feeds…"
msgstr "Импорт лент"

#: qml/FeedListPage.qml:45
#, fuzzy, kde-format
#| msgid "Export Feeds"
msgid "Export Feeds…"
msgstr "Экспорт лент"

#: qml/FeedListPage.qml:69
#, kde-format
msgid "Add Feed…"
msgstr "Добавить ленту…"

#: qml/FeedListPage.qml:83
#, kde-format
msgid "No feeds added yet"
msgstr "Ленты новостей ещё не добавлены"

#: qml/FeedListPage.qml:120
#, kde-format
msgid "Import Feeds"
msgstr "Импорт лент"

#: qml/FeedListPage.qml:122
#, kde-format
msgid "All Files (*)"
msgstr "Все файлы (*)"

#: qml/FeedListPage.qml:122
#, kde-format
msgid "XML Files (*.xml)"
msgstr "Файлы XML (*.xml)"

#: qml/FeedListPage.qml:122
#, kde-format
msgid "OPML Files (*.opml)"
msgstr "Файлы OPML (*.opml)"

#: qml/FeedListPage.qml:128
#, kde-format
msgid "Export Feeds"
msgstr "Экспорт лент"

#: qml/FeedListPage.qml:130
#, kde-format
msgid "All Files"
msgstr "Все файлы"

#: qml/GroupsListPage.qml:18
#, kde-format
msgid "Groups"
msgstr "Группы"

#: qml/GroupsListPage.qml:24
#, kde-format
msgid "Add Group…"
msgstr "Добавить группу…"

#: qml/GroupsListPage.qml:55
#, kde-format
msgid "Remove"
msgstr "Удалить"

#: qml/GroupsListPage.qml:63
#, kde-format
msgid "Set as Default"
msgstr "Установить по умолчанию"

#: qml/GroupsListPage.qml:72
#, kde-format
msgid "No groups created yet"
msgstr "Группы ещё не созданы"

#: qml/GroupsListPage.qml:77
#, kde-format
msgid "Add Group"
msgstr "Добавить группу"

#: qml/SettingsPage.qml:21
#, kde-format
msgid "Article List"
msgstr "Список статей"

#: qml/SettingsPage.qml:25
#, kde-format
msgid "Delete after:"
msgstr "Удалить через:"

#: qml/SettingsPage.qml:38
#, kde-format
msgid "Never"
msgstr "не удалять"

#: qml/SettingsPage.qml:38
#, kde-format
msgid "Articles"
msgstr "(статьи)"

#: qml/SettingsPage.qml:38
#, kde-format
msgid "Days"
msgstr "(дни)"

#: qml/SettingsPage.qml:38
#, kde-format
msgid "Weeks"
msgstr "(недели)"

#: qml/SettingsPage.qml:38
#, kde-format
msgid "Months"
msgstr "(месяцы)"

#: qml/SettingsPage.qml:46
#, kde-format
msgid "Article"
msgstr "Статья"

#: qml/SettingsPage.qml:54
#, kde-format
msgid "Font size:"
msgstr "Размер шрифта:"

#: qml/SettingsPage.qml:65
#, kde-format
msgid "Use system default"
msgstr "Использовать системные параметры по умолчанию"

#~ msgid "Import Feeds..."
#~ msgstr "Импорт лент…"

#~ msgid "Export Feeds..."
#~ msgstr "Экспорт лент…"

#~ msgid "OK"
#~ msgstr "OK"

#~ msgid "Cancel"
#~ msgstr "Отмена"

#~ msgid "Configure Groups"
#~ msgstr "Настройка групп"

#~ msgid "Mark as unread"
#~ msgstr "Отметить как непрочитанное"

#~ msgid "Mark as read"
#~ msgstr "Отметить как прочитанное"

#, fuzzy
#~| msgid "All Entries"
#~ msgid "Show all entries"
#~ msgstr "Все записи"

#, fuzzy
#~| msgid "No unread entries available"
#~ msgid "Show only unread entries"
#~ msgstr "Нет доступных непрочитанных записей"

#~ msgid "Edit"
#~ msgstr "Изменить"

#~ msgid "Feed group"
#~ msgstr "Группа лент новостей"

#~ msgid "Add feed"
#~ msgstr "Добавить ленту"
